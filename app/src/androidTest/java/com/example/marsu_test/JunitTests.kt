package com.example.marsu_test

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.marsu_test.ROOM.FeedEntryDao
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.GeneralProvisionsTable
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.example.marsu_test.db.FeedEntryDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class JunitTests {
    private lateinit var db: FeedEntryDatabase
    private lateinit var dao: FeedEntryDao

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, FeedEntryDatabase::class.java).build()
        dao = db.feedEntryDao
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun testInsertGeneralProvisions() {
        val newGeneralProvisions = listOf( GeneralProvisionsTable("What colors?", listOf("black","white","red"), "2","Общие положения"))
        dao.insertGeneralProvisions(newGeneralProvisions)
        val insertedFeedEntry = dao.readAllTable().find { it.questions == "What colors?" }
        assertNotNull(insertedFeedEntry.toString(), "What colors?")
    }

    @Test
    fun testInsertFeedEntry() {
        val newFeedEntry = listOf( FeedEntry(1, 10, "test.jpg", true,
            "Test", "CategoryTest"))
        dao.insertFeedEntry(newFeedEntry)
        val insertedFeedEntry = dao.readTableFilter(true).find { it.time == 10 }
        assertNotNull(insertedFeedEntry.toString(), 10)
    }

    @Test
    fun testReadFeedEntry() {
        val feedEntryList = dao.readTableQuestions("circle")
        assertTrue(feedEntryList.isNotEmpty().toString(), true)
    }

    @Test
    fun testReadTablee() {
        val feedEntryList = dao.readTablee("circle")
        assertTrue(feedEntryList.isNotEmpty().toString(), true)
    }

    @Test
    fun testReadTableFilter() {
        val feedEntryList = dao.readTableFilter(fla = false)
        assertTrue(feedEntryList.isNotEmpty().toString(), true)
    }

    @Test
    fun testDeleteFeedEntry() {
        val testFeedEntry = listOf( FeedEntry(1, 10, "test.jpg", true,
            "Test", "CategoryTest", 1))
        dao.insertFeedEntry(testFeedEntry)
        val insertedId = testFeedEntry[0].id
        dao.deleteFeedEntry(insertedId)
        assertTrue(dao.readAllTable().isEmpty())
    }

    @Test
    fun testUpdateFeedEntry() {
        val newFeedEntry = listOf( FeedEntry(1, 10, "test.jpg", true,
            "Test", "CategoryTest"))
        dao.insertFeedEntry(newFeedEntry)
        val insertedId = newFeedEntry[0].id+1
        dao.updateFeedEntry(true, insertedId)
        val updatedFeedEntry = dao.readTableFilter(true).find { it.id == insertedId }
        assertNotNull(updatedFeedEntry.toString(), true)
        assertEquals(true, updatedFeedEntry?.flag)
    }

    @Test
    fun categoryFeedEntry() {
        val feedEntryList = dao.categoryFeedEntry(cat = "CategoryTest")
        assertTrue(feedEntryList.isNotEmpty().toString(), true)
    }

}