package com.example.marsu_test

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.marsu_test.ROOM.AnswersTable.DataClassAnswers
import com.example.marsu_test.ROOM.DBHandler
import com.example.marsu_test.ViewPager2.VoprosAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class TestActivity : AppCompatActivity () {
    private lateinit var viewPager: ViewPager2
    val adapter = RecyclerAdapterTest(this)
    private lateinit var recyclerView: RecyclerView
    private lateinit var nextButton: ImageButton
    private lateinit var previusButton: ImageButton
    private lateinit var hedr: TextView
    private val db by lazy { DBHandler.getInstance(this) }
    private lateinit var answers: DataClassAnswers
    private var questionsSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        viewPager = findViewById(R.id.pager)
        recyclerView = findViewById(R.id.recyclerViewTest1)
        val getStr = intent.getStringExtra("nameTheme").toString()
        var iter = 0

        hedr = findViewById(R.id.test_text)
        hedr.text = getStr

        recyclerView.layoutManager = LinearLayoutManager(this , LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = adapter

        answers = DataClassAnswers()
        val ref = this
        lifecycleScope.launch(Dispatchers.IO) {
            val readVopros = db.feedEntryDao.readTablee(getStr)
            val readQuestions = db.feedEntryDao.readTableQuestions(getStr)
            adapter.number = readQuestions.size
            if (readVopros.isEmpty() || readQuestions.isEmpty()){
                val intent = Intent(
                    ref,
                    PlugActivity::class.java
                )
                startActivity(intent)
            }else{
                ref.runOnUiThread {
                    questionsSize = readQuestions.size
                    viewPager.adapter = VoprosAdapter(ref, readVopros, readQuestions, answers)
                }
            }

        }


        val back = findViewById<ImageButton>(R.id.test_image)
        back.setOnClickListener {
            finish()
        }

        nextButton = findViewById(R.id.button_test1)
        previusButton = findViewById(R.id.button_test)

        nextButton.setOnClickListener{
            viewPager.currentItem = viewPager.currentItem + 1
//            println(questionsSize)
//            println(viewPager.currentItem)
//            if (viewPager.currentItem + 1 == questionsSize){
//                val intent = Intent(
//                    this,
//                    HomeActivity::class.java
//                )
//                startActivity(intent)
//            }
        }
        previusButton.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem - 1
        }

    }
   fun onClickCrugTest(position: Int) {
       viewPager.currentItem = position
   }

}