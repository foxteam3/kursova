package com.example.marsu_test

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.marsu_test.ROOM.TestTable.FeedEntry

class RecyclerAdapterHome (private val dataList: List<FeedEntry>, private val activity: HomeActivity) : RecyclerView.Adapter<RecyclerAdapterHome.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imageCardView1: ImageView = itemView.findViewById(R.id.itemImageView)
        val nameTest: TextView = itemView.findViewById(R.id.text_test1)
        val time: TextView = itemView.findViewById(R.id.Time)
        val flag: AppCompatToggleButton = itemView.findViewById(R.id.flag1)
        init {
            val flag = itemView.findViewById<AppCompatToggleButton>(R.id.flag1)
            val cardViewItem = itemView.findViewById<CardView>(R.id.cardViewItem)
            flag.setOnClickListener {
                activity.onClickFlag(dataList[absoluteAdapterPosition])
            }
            cardViewItem.setOnClickListener {
                activity.onClickCardItem(dataList[absoluteAdapterPosition])
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_home, parent, false)
        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = dataList[position]
        Glide.with(holder.itemView).load(currentItem.image).into(holder.imageCardView1)
        holder.nameTest.text = currentItem.title
        holder.time.text = currentItem.time.toString() + " min"
        holder.flag.isChecked = currentItem.flag
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}