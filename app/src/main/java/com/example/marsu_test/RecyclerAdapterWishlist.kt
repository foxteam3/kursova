package com.example.marsu_test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.marsu_test.ROOM.TestTable.FeedEntry


class RecyclerAdapterWishlist(private var dataList: List<FeedEntry>, private val activity: WishlistActivity) : RecyclerView.Adapter<RecyclerAdapterWishlist.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imageCardView1: ImageView = itemView.findViewById(R.id.itemImageView)
        val nameTest: TextView = itemView.findViewById(R.id.nazvanie_1)
        val numberOfQuestions: TextView = itemView.findViewById(R.id.textViewQuestion)
        val time: TextView = itemView.findViewById(R.id.textViewTime)
        init {
            val delete = itemView.findViewById<ImageView>(R.id.delete)
            val button = itemView.findViewById<Button>(R.id.buttonWishlist)
            delete.setOnClickListener {
                activity.onClickUpdateFalse(dataList[absoluteAdapterPosition])
                removeItem(absoluteAdapterPosition)
            }
            button.setOnClickListener {
                activity.onClickButtonStart(dataList[absoluteAdapterPosition])
            }
        }
   }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_wishlist, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = dataList[position]
        Glide.with(holder.itemView).load(currentItem.image).into(holder.imageCardView1)
        holder.nameTest.text = currentItem.title
        holder.numberOfQuestions.text = currentItem.numberOfQuestions.toString()
        holder.time.text = currentItem.time.toString()
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
    fun removeItem(position: Int) {
        dataList = dataList - dataList[position]
        notifyDataSetChanged()
    }
}