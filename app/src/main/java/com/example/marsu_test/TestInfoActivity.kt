package com.example.marsu_test

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.lifecycle.lifecycleScope
import com.example.marsu_test.ROOM.Object
import com.example.marsu_test.ROOM.DBHandler
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TestInfoActivity : AppCompatActivity() {
    private val db by lazy { DBHandler.getInstance(this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_info)
        val startButton = findViewById<Button>(R.id.buttonStart)
        val imageView = findViewById<ImageView>(R.id.imageView3)
        val back = findViewById<ImageButton>(R.id.imageViewBack)
        val nameTestHeader = findViewById<TextView>(R.id.textViewNameHeader)
        val numberOfQuestions = findViewById<TextView>(R.id.textViewQuestion)
        val time = findViewById<TextView>(R.id.textViewTime)
        val win = findViewById<TextView>(R.id.textViewWin)
        val nameTest = findViewById<TextView>(R.id.textViewNameTest)
        val flagTestButton = findViewById<AppCompatToggleButton>(R.id.flagStartTestInfo)

        @Suppress("DEPRECATION")
        val objectExtra = intent.getSerializableExtra("object") as Object

        val getBool = intent.getBooleanExtra("backTestInfo", false)

        Picasso.with(this).load(objectExtra.image).into(imageView)

        nameTestHeader.text = objectExtra.title
        nameTest.text = objectExtra.title
        numberOfQuestions.text = objectExtra.numberOfQuestions.toString() + " Question"
        time.text = objectExtra.time.toString() + " min"
        win.text = "Win " + objectExtra.numberOfQuestions.toString() + " star"

        back.setOnClickListener {
            if (getBool){
                val intent = Intent(
                    this,
                    HomeActivity::class.java
                )
                startActivity(intent)
            } else{
                val intent = Intent(
                    this,
                    WishlistActivity::class.java
                )
                startActivity(intent)
            }
        }

        startButton.setOnClickListener {
            val intent = Intent(
                this,
                TestActivity::class.java
            )
            intent.putExtra("nameTheme", objectExtra.title)
            startActivity(intent)
        }

        flagTestButton.isChecked = objectExtra.flag
        flagTestButton.setOnClickListener {
            objectExtra.flag = !objectExtra.flag
            lifecycleScope.launch(Dispatchers.IO) {
                db.feedEntryDao.updateFeedEntry(objectExtra.flag, objectExtra.id)
            }
        }

    }
}