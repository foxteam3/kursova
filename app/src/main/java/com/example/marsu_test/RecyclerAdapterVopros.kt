package com.example.marsu_test

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.GeneralProvisionsTable
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.example.marsu_test.ViewPager2.VoprosFragment


class RecyclerAdapterVopros(private val dataList: List<String>, private val fragment: VoprosFragment): RecyclerView.Adapter<RecyclerAdapterVopros.MyViewHolder>() {
    private var selectedItemPosition: Int = -1

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val numberVopros = itemView.findViewById<TextView>(R.id.test_text9)
        val textVopros = itemView.findViewById<TextView>(R.id.test_text10)
        val clickCardView = itemView.findViewById<CardView>(R.id.testRightAnswers)
        init {
            itemView.setOnClickListener {
                fragment.clickCardView(absoluteAdapterPosition)
                val oldSelectedItemPosition = selectedItemPosition
                selectedItemPosition = absoluteAdapterPosition
                clickCardView.setCardBackgroundColor(Color.parseColor("#F7F0FF"))
                textVopros.setTextColor(Color.parseColor("#841FFD"))
                if (oldSelectedItemPosition != -1 && oldSelectedItemPosition != selectedItemPosition) {
                    notifyItemChanged(oldSelectedItemPosition)
                }
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_item_vopros, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = dataList[position]
        holder.numberVopros.text = (position + 1).toString() + "."
        holder.textVopros.text = currentItem
        if (position == selectedItemPosition) {
            holder.clickCardView.setCardBackgroundColor(Color.parseColor("#F7F0FF"))
            holder.textVopros.setTextColor(Color.parseColor("#841FFD"))
        } else {
            holder.clickCardView.setCardBackgroundColor(Color.WHITE)
            holder.textVopros.setTextColor(Color.BLACK)
        }
    }
    override fun getItemCount(): Int {
        return dataList.size
    }

}