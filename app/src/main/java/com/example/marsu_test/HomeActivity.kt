package com.example.marsu_test

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marsu_test.ROOM.TestTable.CategoryEnum
import com.example.marsu_test.ROOM.Object
import com.example.marsu_test.ROOM.DBHandler
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeActivity : AppCompatActivity() {
   private val db by lazy { DBHandler.getInstance(this) }
   private lateinit var bottomNavigationView: BottomNavigationView

    private val mOnNavigationItemSelectedListener=
        NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.search -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.exams -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.wishlist -> {
                    val intent = Intent(this, WishlistActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.account -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }
            }
            false
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_main)
        findViewById<NavigationBarView>(R.id.bottomNavigationView).setOnItemSelectedListener(mOnNavigationItemSelectedListener)


        val recyclerView1: RecyclerView = findViewById(R.id.recyclerView2)
        val recyclerViewTop: RecyclerView = findViewById(R.id.recyclerViewTop)
        val recyclerViewPopular: RecyclerView = findViewById(R.id.recyclerViewPopular)
        val recyclerViewCourses: RecyclerView = findViewById(R.id.recyclerViewCourses)
        recyclerView1.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewTop.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewPopular.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewCourses.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val ref = this
        lifecycleScope.launch(Dispatchers.IO) {
            val readRecommendDb = db.feedEntryDao.categoryFeedEntry(CategoryEnum.Recommend.toString())
            val readTopDb = db.feedEntryDao.categoryFeedEntry(CategoryEnum.Top.toString())
            val readPopularDb = db.feedEntryDao.categoryFeedEntry(CategoryEnum.Popular.toString())
            val readCoursesDb = db.feedEntryDao.categoryFeedEntry(CategoryEnum.Courses.toString())
            ref.runOnUiThread {
                recyclerView1.adapter = RecyclerAdapterHome(readRecommendDb, ref)
                recyclerViewTop.adapter = RecyclerAdapterHome(readTopDb, ref)
                recyclerViewPopular.adapter = RecyclerAdapterHome(readPopularDb, ref)
                recyclerViewCourses.adapter = RecyclerAdapterHome(readCoursesDb, ref)
            }
        }
    }


    fun onClickFlag(feedEntry: FeedEntry){
        feedEntry.flag = !feedEntry.flag
        lifecycleScope.launch(Dispatchers.IO) {
            db.feedEntryDao.updateFeedEntry(feedEntry.flag, feedEntry.id)
        }
    }

    fun onClickCardItem(subject: FeedEntry){
        val intent = Intent(
            this,
            TestInfoActivity::class.java
        )
        val getObject = Object(
            subject.numberOfQuestions,
            subject.time,
            subject.image,
            subject.flag,
            subject.title,
            subject.category,
            subject.id
        )
        intent.putExtra("object", getObject)
        intent.putExtra("backTestInfo", true)
        startActivity(intent)
    }
}