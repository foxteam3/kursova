package com.example.marsu_test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapterTest (private val activity: TestActivity): RecyclerView.Adapter<RecyclerAdapterTest.MyViewHolder>() {
    var number = 0
        set(value){
            field = value
            notifyDataSetChanged()
        }
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val testText = itemView.findViewById<TextView>(R.id.test_text2)
        init {
            val crugTest = itemView.findViewById<ImageView>(R.id.test_crug)
            crugTest.setOnClickListener {
                activity.onClickCrugTest(absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapterTest.MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_test, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.testText.text = (position + 1).toString()
    }

    override fun getItemCount(): Int {
        return number
    }
}