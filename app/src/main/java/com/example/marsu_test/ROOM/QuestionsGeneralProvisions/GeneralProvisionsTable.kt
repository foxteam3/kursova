package com.example.marsu_test.ROOM.QuestionsGeneralProvisions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity
data class GeneralProvisionsTable(
    val questions: String,

    val listAnswers: List<String>,
    val rightAnswers: String,
    val theme: String,


    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)