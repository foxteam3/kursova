package com.example.marsu_test.ROOM.QuestionsGeneralProvisions

import androidx.room.TypeConverter
import java.util.Arrays
import java.util.stream.Collectors


class HobbiesConverter {

    @TypeConverter
    fun fromHobbies(hobbies: List<String?>): String {
        return hobbies.stream().collect(Collectors.joining("/"))
    }

    @TypeConverter
    fun toHobbies(data: String): List<String> {
        return Arrays.asList(*data.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
    }
}