package com.example.marsu_test.ROOM.AnswersTable

data class DataClassAnswers (
    var firstAnswers: Int = 0,
    var secondAnswers: Int = 0,
    var thirdAnswers: Int = 0,
    var fourthAnswers: Int = 0,
    var fifthAnswers: Int = 0,
){}