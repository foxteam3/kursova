package com.example.marsu_test.ROOM

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.GeneralProvisionsTable
import com.example.marsu_test.ROOM.TestTable.CategoryEnum
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.example.marsu_test.db.FeedEntryDatabase
import kotlin.concurrent.thread

object DBHandler {

    @Volatile var INSTANCE: FeedEntryDatabase? = null

    fun getInstance(context: Context): FeedEntryDatabase = INSTANCE ?: synchronized(this) {
        INSTANCE ?: getDB(context).also { INSTANCE = it }
    }
    fun getDB(context: Context): FeedEntryDatabase {

        val db = Room.databaseBuilder(context, FeedEntryDatabase::class.java, "FeedEntryDatabase").addCallback(
            object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    thread {
                        getInstance(context).feedEntryDao.insertFeedEntry(PREPOPULATE_DATA)
                        getInstance(context).feedEntryDao.insertGeneralProvisions(GENERAlPR)
                    }
                }
            }).build()
        return  db
    }
    val PREPOPULATE_DATA = listOf(
        FeedEntry(id = 1,title = "Общие положения", numberOfQuestions = 4, time = 10, image =  "https://media.istockphoto.com/id/527864331/ru/%D0%B2%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%BD%D0%B0%D1%8F/%D0%BE%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%B0%D1%8F-%D0%BA%D0%BD%D0%B8%D0%B3%D0%B0-%D0%B2-%D0%BE%D1%82%D1%82%D0%B5%D0%BD%D0%BA%D0%B8-%D1%81%D0%B5%D1%80%D0%BE%D0%B3%D0%BE-%D1%86%D0%B2%D0%B5%D1%82%D0%B0.jpg?s=612x612&w=0&k=20&c=h4BgSScgcB3r1HjibA9wZUg8jMcPTjW4xRLYY3reQHo=", flag =  false, category = CategoryEnum.Recommend.toString()),
        FeedEntry(id = 2,title = "Дорожные знаки", numberOfQuestions = 5, time = 10, image ="https://avatars.mds.yandex.net/i?id=9ec6221c9ed6f3ab9763f69784d7b884a34b1196-3193964-images-thumbs&n=13", flag = false, category = CategoryEnum.Recommend.toString()),
        FeedEntry(id = 3, title = "Дорожная разметка", numberOfQuestions = 3,  time = 10, image ="https://avatars.mds.yandex.net/i?id=34c3ef2de241393201699b1f1d0c4537936e0cd7-5329555-images-thumbs&ref=rim&n=33&w=300&h=200", flag = false, category = CategoryEnum.Recommend.toString()),
        FeedEntry(id = 4, title = "Сигналы светофора", numberOfQuestions = 5,  time = 10, image ="https://avatars.mds.yandex.net/i?id=89919ffc56261a629d50152e23a7c7465a061d65-12168264-images-thumbs&n=13", flag = false, category = CategoryEnum.Top.toString()),
        FeedEntry(id = 5, title = "Начало движения", numberOfQuestions = 5,  time = 10, image ="https://avatars.mds.yandex.net/i?id=b0c2f8551f61c326d980eecce418134cb4d82770-8959338-images-thumbs&n=13", flag = false, category = CategoryEnum.Top.toString()),
        FeedEntry(id = 6, title = "Скорость движения", numberOfQuestions = 5,  time = 10, image ="https://avatars.mds.yandex.net/i?id=28b0fd493e15cfb442d55eff42091eb9412d0bfb-12422657-images-thumbs&n=13", flag = false, category = CategoryEnum.Top.toString()),
        FeedEntry(id = 7, title = "Обгон, опережение", numberOfQuestions = 5,  time = 10, image ="https://avatars.mds.yandex.net/i?id=5107e76e974de3de912bf2b47ebc0fef331fec3f-10555755-images-thumbs&n=13", flag = false, category = CategoryEnum.Popular.toString()),
        FeedEntry(id = 8, title = "Остановка и стоянка", numberOfQuestions = 5,  time = 10, image ="https://i.pinimg.com/564x/68/77/75/68777543bcd16992661d8758e7731973.jpg", flag = false, category = CategoryEnum.Popular.toString()),
        FeedEntry(id = 9, title = "Общие обязанности", numberOfQuestions = 5,  time = 10, image ="https://i.pinimg.com/564x/f4/19/6a/f4196ac5dc0d368c85d54e0b3c5c90d2.jpg", flag = false, category = CategoryEnum.Popular.toString()),
        FeedEntry(id = 10, title = "Движение в жилых зонах", numberOfQuestions = 5,  time = 10, image ="https://i.pinimg.com/564x/9a/cc/21/9acc21037533fb9c78a4b6c7a82ab2ee.jpg", flag = false, category = CategoryEnum.Courses.toString()),
        FeedEntry(id = 11, title = "Перевозка людей и грузов", numberOfQuestions = 5,  time = 10, image ="https://i.pinimg.com/564x/99/e3/60/99e3609ea0971ba8bfc9e68ca9d9536d.jpg", flag = false, category = CategoryEnum.Courses.toString()),
        FeedEntry(id = 12, title = "Ответственность водителя", numberOfQuestions = 5,  time = 10, image ="https://i.pinimg.com/736x/f6/8f/e8/f68fe8ca77b0efe1b5c9738bd69d63c0.jpg", flag = false, category = CategoryEnum.Courses.toString())
    )
    val GENERAlPR = listOf(
        GeneralProvisionsTable("В каком случае водитель совершит вынужденную остановку?", listOf("Остановившись непосредственно перед пешеходным переходом, чтобы уступить дорогу пешеходу", "Остановившись на проезжей части из-за технической неисправности транспортного средства", "В обоих перечисленных случаях"), "2", "Общие положения"),
        GeneralProvisionsTable("Какие из предупреждающих и запрещающих знаков являются временными?", listOf("Только установленные на переносных опорах", "Имеющие желтый фон, а также установленные на переносных опорах", "Только установленные в местах производства дорожных работ"), "2", "Дорожные знаки"),
        GeneralProvisionsTable("Где начинают действовать требования Правил, относящиеся к населённым пунктам?", listOf("Только с места установки дорожного знака «Начало населенного пункта» на белом фоне", "С места установки дорожного знака с названием населённого пункта на белом или синем фоне", "В начале застроенной территории, непосредственно прилегающей к дороге"), "1", "Дорожные знаки"),
        GeneralProvisionsTable("Разрешен ли Вам поворот на дорогу с грунтовым покрытием?", listOf("Разрешен", "Разрешен только при технической неисправности транспортного средства", "Запрещен"), "1", "Дорожные знаки"),
        GeneralProvisionsTable("Этот знак предупреждает о приближении к тоннелю, в котором", listOf("Будет затруднён разъезд со встречными транспортными средствами", "Отсутствует искусственное освещение", "Очерёдность движения регулируется светофором"), "2", "Дорожные знаки"),
        GeneralProvisionsTable("В зоне действия этого знака разрешается использовать звуковой сигнал:", listOf("Только для предупреждения об обгоне", "Только для предотвращения дорожно-транспортного происшествия", "В обоих перечисленных случаях"), "2", "Дорожные знаки"),
        GeneralProvisionsTable("Что называется разрешённой максимальной массой транспортного средства?", listOf("Максимально допустимая для перевозки масса груза, установленная предприятием-изготовителем", "Масса снаряженного транспортного средства без учета массы водителя, пассажиров и груза, установленная предприятием-изготовителем", "Масса снаряженного транспортного средства с грузом, водителем и пассажирами, установленная предприятием-изготовителем в качестве максимально допустимой"), "3", "Общие положения"),
        GeneralProvisionsTable("Являются ли тротуары и обочины частью дороги?", listOf("Являются", "Являются только обочины", "Не являются"), "1", "Общие положения"),
        GeneralProvisionsTable("Главной на перекрестке является:", listOf("Дорога с твердым покрытием по отношению к грунтовой дороге", "Дорога с асфальтобетонным покрытием по отношению к дороге, покрытой брусчаткой", "Дорога с тремя или более полосами движения по отношению к дороге с двумя полосами"), "1", "Общие положения"),
        GeneralProvisionsTable("Что означает разметка в виде надписи «СТОП» на проезжей части?", listOf("Предупреждает о приближении к стоп-линии перед регулируемым перекрёстком", "Предупреждает о приближении к стоп-линии и знаку «Движение без остановки запрещено»", "Предупреждает о приближении к знаку «Уступите дорогу»"), "2", "Дорожная разметка"),
        GeneralProvisionsTable("Чем необходимо руководствоваться, если значения дорожных знаков и линий горизонтальной разметки противоречат друг другу?", listOf("Требованиями линий разметки", "Требованиями дорожных знаков", "Правила эту ситуацию не регламентируют"), "2", "Дорожная разметка"),
        GeneralProvisionsTable("Чем необходимо руководствоваться, если нанесённые на проезжей части белые и оранжевые линии разметки противоречат друг другу?", listOf("Белыми линиями разметки", "Оранжевыми линиями разметки", "Правила эту ситуацию не регламентируют"), "2", "Дорожная разметка"),
    )
}