package com.example.marsu_test.ROOM.TestTable

enum class CategoryEnum {
    Recommend,
    Top,
    Popular,
    Courses,
}