package com.example.marsu_test.ROOM

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.GeneralProvisionsTable
import com.example.marsu_test.ROOM.TestTable.FeedEntry

@Dao
interface FeedEntryDao{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFeedEntry(feedEntry: List<FeedEntry>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGeneralProvisions(feedEntry: List<GeneralProvisionsTable>)

    @Query("SELECT listAnswers FROM 'GeneralProvisionsTable' WHERE theme = :th")
    fun readTablee(th: String): List<String>

    @Query("SELECT questions FROM 'GeneralProvisionsTable' WHERE theme = :th")
    fun readTableQuestions(th: String): List<String>

    @Query("SELECT * FROM 'GeneralProvisionsTable'")
    fun readAllTable(): List<GeneralProvisionsTable>

    @Query("SELECT * FROM 'FeedEntry' WHERE flag = :fla")
    fun readTableFilter(fla: Boolean): List<FeedEntry>

   @Query("Delete from 'FeedEntry' where id = :subId")
   fun deleteFeedEntry(subId: Int)

    @Query("Update 'FeedEntry' Set flag = :flag where id = :subId")
    fun updateFeedEntry(flag: Boolean, subId: Int)

    @Query("SELECT * FROM 'FeedEntry' WHERE category = :cat")
    fun categoryFeedEntry(cat: String): List<FeedEntry>
}