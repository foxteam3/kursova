package com.example.marsu_test.ROOM

import androidx.room.PrimaryKey
import java.io.Serializable

data class Object(
    var numberOfQuestions: Int = 0,
    var time: Int = 0,
    var image: String = "",
    var flag: Boolean = false,
    var title: String = "",
    var category: String = "",
    var id: Int = 0
): Serializable{}
