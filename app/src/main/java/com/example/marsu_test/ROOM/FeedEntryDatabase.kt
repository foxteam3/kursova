package com.example.marsu_test.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.GeneralProvisionsTable
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.HobbiesConverter
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.example.marsu_test.ROOM.FeedEntryDao


@Database(
    entities = [FeedEntry::class, GeneralProvisionsTable::class],
    version = 1
)
@TypeConverters(HobbiesConverter::class)
abstract class FeedEntryDatabase: RoomDatabase() {

    abstract val feedEntryDao: FeedEntryDao

}