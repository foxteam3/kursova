package com.example.marsu_test.ROOM.TestTable

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FeedEntry (
    val numberOfQuestions: Int,
    val time: Int,
    val image: String,
    var flag: Boolean,
    val title: String,
    val category: String,

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)