package com.example.marsu_test

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class PlugActivity : AppCompatActivity() {
    private lateinit var buttonHome: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plug)

        buttonHome = findViewById(R.id.buttonPlug)
        buttonHome.setOnClickListener {
            val intent = Intent(
                this,
                HomeActivity::class.java
            )
            startActivity(intent)
        }

    }
}