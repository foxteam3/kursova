package com.example.marsu_test

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marsu_test.ROOM.Object
import com.example.marsu_test.ROOM.DBHandler
import com.example.marsu_test.ROOM.TestTable.FeedEntry
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WishlistActivity : AppCompatActivity() {
   private val db by lazy { DBHandler.getInstance(this) }

    private val mOnNavigationItemSelectedListener=
        NavigationBarView.OnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.wishlist -> {
                    val intent = Intent(this, WishlistActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.search -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.exams -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.home -> {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    true
                }

                R.id.account -> {
                    val intent = Intent(this, PlugActivity::class.java)
                    startActivity(intent)
                    true
                }
            }
            false
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wishlist)
        findViewById<NavigationBarView>(R.id.bottomNavigationView).setOnItemSelectedListener(mOnNavigationItemSelectedListener)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val ref = this
        lifecycleScope.launch(Dispatchers.IO) {
            val readDb = db.feedEntryDao.readTableFilter(true)
            ref.runOnUiThread {
                recyclerView.adapter = RecyclerAdapterWishlist(readDb, ref)
            }
        }
    }

    fun  onClickUpdateFalse(subject: FeedEntry){
        subject.flag = !subject.flag
        lifecycleScope.launch(Dispatchers.IO) {
            db.feedEntryDao.updateFeedEntry(subject.flag, subject.id)
        }
    }

    fun  onClickButtonStart(subject: FeedEntry){
        val intent = Intent(
            this,
            TestInfoActivity::class.java
        )
        val getObject = Object(
            subject.numberOfQuestions,
            subject.time,
            subject.image,
            subject.flag,
            subject.title,
            subject.category,
            subject.id
        )
        intent.putExtra("object", getObject)
        intent.putExtra("backTestInfo", false)
        startActivity(intent)
    }
}