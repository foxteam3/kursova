package com.example.marsu_test.ViewPager2

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.marsu_test.ROOM.AnswersTable.DataClassAnswers
import com.example.marsu_test.ROOM.QuestionsGeneralProvisions.HobbiesConverter

class VoprosAdapter(fragment: FragmentActivity, val stroka: List<String>, val questions: List<String>, val answer: DataClassAnswers): FragmentStateAdapter(fragment) {
    private  val con by lazy { HobbiesConverter() }

    override fun getItemCount(): Int = questions.size

    override fun createFragment(position: Int): Fragment {
        val pars = con.toHobbies(stroka[position])
        val fragment = VoprosFragment(pars, position, answer)
        fragment.arguments = Bundle().apply {
            putString(ARG_OBJECT,  questions[position])
        }
        return fragment
    }
}