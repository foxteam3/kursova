package com.example.marsu_test.ViewPager2


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marsu_test.R
import com.example.marsu_test.ROOM.AnswersTable.DataClassAnswers
import com.example.marsu_test.RecyclerAdapterVopros


const val ARG_OBJECT = "object"


class VoprosFragment(val stroka: List<String>, val posit: Int, val answer: DataClassAnswers) : Fragment() {
    private lateinit var recyclerAdapterVopros1: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_vopros, container, false)
        recyclerAdapterVopros1 = view.findViewById(R.id.recyclerAdapterVopros1)
        recyclerAdapterVopros1.layoutManager = LinearLayoutManager(context)
        recyclerAdapterVopros1.adapter = RecyclerAdapterVopros(stroka, this)

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_OBJECT) }?.apply {
            val textView: TextView = view.findViewById(R.id.text_test)
            textView.text = getString(ARG_OBJECT)
        }

    }

    fun clickCardView(position: Int){
        when (posit){
            0 -> answer.firstAnswers = position + 1
            1 -> answer.secondAnswers = position + 1
            2 -> answer.thirdAnswers = position + 1
            3 -> answer.fourthAnswers = position + 1
            4 -> answer.fifthAnswers = position + 1
            else -> println("error")
        }
    }


}
